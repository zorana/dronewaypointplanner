#ifndef DRONEARUCOPLANNER_H
#define DRONEARUCOPLANNER_H

#include <string>

#include "ros/ros.h"
#include "droneModuleROS.h"
#include "communication_definition.h"
//#include "missionScheduling.h"
//#include "spaceMap.h"
#include "observation3d.h"
//#include "droneTrajectoryControllerROSModule.h"
#include "droneTrajectoryController.h"

//Drone msgs
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/droneAltitudeCmd.h"
#include "droneMsgsROS/obsVector.h"

// CVG utils
//#include "xmlfilereader.h"
//#include "cvg_string_conversions.h"
#include "droneMsgsROS/setControlMode.h"


//Services
#include "droneMsgsROS/setControlMode.h"
#include "droneloggerrospublisher.h"
#include "control/Controller_MidLevel_controlModes.h"


//dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <droneTrajectoryControllerROSModule/trajectoryControllerConfig.h>

//interface
#include "droneekfstateestimatorinterface.h"
#include "communication_definition.h"
#include "dronetrajectorycontrollerinterface.h"
#include "dronemoduleinterface.h"
#include "droneMsgsROS/droneCommand.h"

//#include "droneInterfaceROSModule.h"

#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PointStamped.h>


//Aruco Messages
#include "aruco_eye_msgs/PointInImage.h"
#include "aruco_eye_msgs/Marker.h"
#include "aruco_eye_msgs/MarkerList.h"



//Console
#include <curses.h>


using namespace std;

class DroneArucoPlannerROSModule : public DroneModule
{
public:
  DroneArucoPlannerROSModule();
  ~DroneArucoPlannerROSModule();

  //DroneModule stuff: reset, start, stop, run, init, open, close
private:
  bool init();
  void close();
protected: ///ove tri prave problem
  //  bool resetValues();
  // bool startVal();
  // bool stopVal();
public:
  bool run(); ///pravi problem
  void open(ros::NodeHandle & nIn);
protected:
  void readParameters();
public:
  bool readConfigs(std::string configFile);
  void setReference(const nav_msgs::Odometry &msg);

  //void proba(droneMsgsROS::dronePositionRefCommandStamped position_ref);
private:


  //private:
  //   // DroneArucoPlannerROSModule drone_trajectory_controller;
  //    //bool setControlModeVal(Controller_MidLevel_controlMode::controlMode mode);
  //    //bool setControlMode(Controller_MidLevel_controlMode::controlMode mode);
  //   // Controller_MidLevel_controlMode::controlMode control_mode;

  // DroneEKFStateEstimatorInterface drone_ekf_state_estimator_interface;//(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR, ModuleNames::ODOMETRY_STATE_ESTIMATOR);
  // drone_ekf_state_estimator_interface.open(n);

  bool isModuleStarted(string);
  bool startModule(bool, string);
  //ros::ServiceClient startClientSrv;
  std_srvs::Empty    emptySrv;
  std::string       module_name_str, module_name_ekf, module_name_traj, module_name_aruco;

  /// Services ///
  ros::ServiceClient moduleIsStartedClientSrv;
  ros::ServiceClient setControlModeClientSrv;
  ros::ServiceClient startArucoModuleClientSrv;

  bool setControlMode(Controller_MidLevel_controlMode::controlMode new_control_mode);


  //drone_trajectory_controller_interface.open(n);

  // DroneInterfaceROSModule drone_interface;



  //public:
  //   // Controller_MidLevel_controlMode::controlMode getControlMode() { return control_mode; }


private:
  std::string drone_arucos_config_file;
  int counter;

  ////    // [Subscribers] Controller references
  //private:
  std::string droneEstimatedPoseTopicName;
  ros::Subscriber droneEstimatedPoseSubs;
  void droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg);
  droneMsgsROS::dronePose last_estimatedPose;

  droneMsgsROS::droneCommand droneCommandMsgs;
  ros::Publisher droneCommandPubl;
  bool publishDroneCmd();

  ros::Publisher distanceFromObjectPub;
  geometry_msgs::PointStamped distanceFromObjectMsg;
  geometry_msgs::PointStamped distanceFromObjectLastMeasurement;

  std::string droneLedLightsPoseEstimationTopicName;
  ros::Subscriber droneLedPoseSubs;
  void droneLedPoseEstimationCallback(const nav_msgs::Odometry& msg);


  //    // [Publishers] Controller references, meaning of useful values depend on the current control mode
  //    // useful values: x, y, z, yaw
  //    std::string drone_position_reference_topic_name;
  //    ros::Publisher drone_position_reference_publisher;
  //    droneMsgsROS::dronePose   current_drone_position_reference;

  //    // useful values: current trajectory: initial_checkpoint,isPeridioc, checkpoints[]
  //    std::string drone_trajectory_reference_topic_name;
  //    ros::Publisher drone_trajectory_reference_publisher;
  //    droneMsgsROS::dronePositionTrajectoryRefCommand current_drone_trajectory_command;

  //private:
  //    //AruCo List (Subscriber)
  std::string droneArucoObservationTopicName;
  ros::Subscriber droneArucoObservationSub;
  //void droneArucoObservationCallback(const droneMsgsROS::obsVector &msg);
  void droneArucoObservationCallback(const aruco_eye_msgs::MarkerListConstPtr& arucoList);
  std::vector<Observation3D> arucoObservations;
  int goalAruco, arucoLeft, arucoRight, arucoOpposite;
  double goalDistance;

  //    // Publishers
  //private:

  //    //drone Mission point (Publisher)
  std::string droneMissionPointCommandTopicName;
  droneMsgsROS::dronePositionRefCommand droneMissionPointCommandMsg;
  ros::Publisher droneMissionPointCommandPub;
  bool publishMissionPointCommand(droneMsgsROS::dronePositionRefCommand droneMissionPointCommandIn);

  //    //ros::Publisher dronePositionRefsPub;
  //    //droneMsgsROS::
  Observation3D observation;

  std::string dronePointToLookTopicName;
  droneMsgsROS::dronePositionRefCommand dronePointToLookMsg;
  ros::Publisher dronePointToLookPub;
  bool publishPointToLook(droneMsgsROS::dronePositionRefCommand dronePointToLookIn);

  ros::Subscriber droneTrajectoryAbsRefSub;
  void publishDroneAbsTrajectoryReference(const std::vector<SimpleTrajectoryWaypoint> * const trajectory_waypoints_out, const int initial_checkpoint_out, const bool is_periodic_out );
  ros::Publisher drone_abs_trajectory_reference_publisher;


////    //std::string controlModeTopicName;
////    //ros::Publisher controlModePub;
////  //  void publishControlMode();


////    // Service servers
////private:
//    std::string setControlModeServiceName;
//ros::ServiceServer setControlModeServerSrv;
//bool setControlModeServCall(droneMsgsROS::setControlMode::Request& request, droneMsgsROS::setControlMode::Response& response);

//    // Drone Logger
//   // DroneLoggerROSPublisher drone_logger_ros_publisher;


};

#endif // DRONEARUCOPLANNER_H
