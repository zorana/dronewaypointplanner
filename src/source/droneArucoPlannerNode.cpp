
#include <iostream>
#include <math.h>

#include "ros/ros.h"
#include "droneArucoPlanner.h"
//#include "droneArucoPlanner.cpp"

int main(int argc,char **argv)
{

  //Ros Init
  ros::init(argc, argv, "droneArucoPlanner");
  ros::NodeHandle n;

  //std::cout << "[ROSNODE] Starting "<<ros::this_node::getName() << std::endl;

  DroneArucoPlannerROSModule MyDroneArucoPlanner;
  MyDroneArucoPlanner.open(n);

  //    DroneEKFStateEstimatorInterface drone_ekf_state_estimator_interface(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR, ModuleNames::ODOMETRY_STATE_ESTIMATOR);
  //    drone_ekf_state_estimator_interface.open(n);

  //    DroneTrajectoryControllerInterface drone_trajectory_controller_interface(MODULE_NAME_TRAJECTORY_CONTROLLER, ModuleNames::TRAJECTORY_CONTROLLER);
  //    drone_trajectory_controller_interface.open(n);



  try
  {

    while(ros::ok())
      {

        //ROS_INFO_STREAM("ros::ok()");

        //Read messages
        ros::spinOnce();
        if(MyDroneArucoPlanner.run())
          {
           // ROS_INFO_STREAM("Module run...");
          }
        else
          {
            //ROS_ERROR_STREAM("Error while running... ");
          }
        //Sleep
//        MyDroneArucoPlanner.sleep();
      }
  }
  catch (std::exception &ex)
  {
    std::cout << "[ROSNODE] Exception :" << ex.what() << std::endl;
  }

  return 1;
}
