#include "droneArucoPlanner.h"

DroneArucoPlannerROSModule::DroneArucoPlannerROSModule()
{
  std::cout << "(..), enter and exit " << std::endl;
  return;
}

DroneArucoPlannerROSModule::~DroneArucoPlannerROSModule()
{
  close();
  return;
}

bool DroneArucoPlannerROSModule::readConfigs(std::string arucoConfigFile)
{

  try
  {
    //std::list<int> visualMarkersList;
    XMLFileReader my_xml_reader(arucoConfigFile);
    goalAruco = my_xml_reader.readIntValue("goal_position:arucoId");
    goalDistance = my_xml_reader.readDoubleValue("goal_position:distance");
    //    arucoLeft = my_xml_reader.readIntValue("other_arucos:arucoLeftId");
    //    arucoRight = my_xml_reader.readIntValue("other_arucos:arucoRightId");
    //    arucoOpposite = my_xml_reader.readIntValue("other_arucos:arucoOppositeId");
    std::cout<<" xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx "<<goalAruco<<" xxxxxxxxxxxxxxxxxxxxxxxxx "<<goalDistance<<std::endl;
  }
  catch (cvg_XMLFileReader_exception &e)
  {
    throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
  }
  return false;
}


bool DroneArucoPlannerROSModule::init()
{
  readConfigs(stackPath+"configs/drone"+cvg_int_to_string(idDrone)+"/"+drone_arucos_config_file);
  counter = 0;
  module_name_ekf = MODULE_NAME_ODOMETRY_STATE_ESTIMATOR;
  module_name_traj = MODULE_NAME_TRAJECTORY_CONTROLLER;
  module_name_aruco = MODULE_NAME_ARUCO_EYE;


  //  if (!isModuleStarted(module_name_aruco))
  //    startModule(true,module_name_aruco);



  return true;
}

void DroneArucoPlannerROSModule::close()
{
  /* DroneModule::~DroneModule gets called automatically */
}

bool DroneArucoPlannerROSModule::run()
{

  //  if (!isModuleStarted(module_name_aruco))
  //    startModule(true,module_name_aruco);

  if(!DroneModule::run())
    {
      std::cout<<"DroneModule not started yet!"<<endl; //TODO obrisi komentar
      return false;
    }
  else
    return true;

  //DroneModule::run();
  //publishControlMode();

  //  if (!isStarted())
  //    {
  //      cout<<"ppppp+++++++++++++ppppppppp"<<endl<<endl;

  //      return false;
  //    }
  //  cout<<"ppppppppppppppppppppp"<<endl<<endl;

  //  return true;
}


void DroneArucoPlannerROSModule::readParameters()
{
  //Configs

  ros::param::get("~drone_arucos_config_file", drone_arucos_config_file);
  if ( drone_arucos_config_file.length() == 0)
    {
      drone_arucos_config_file="drone_arucos_config.xml";
    }
  std::cout<<"drone_arucos_config_file="<<drone_arucos_config_file<<std::endl;

  // Topics
  //
  ros::param::get("~drone_estimated_pose_topic_name", droneEstimatedPoseTopicName);
  if ( droneEstimatedPoseTopicName.length() == 0)
    {
      droneEstimatedPoseTopicName="EstimatedPose_droneGMR_wrt_GFF";
    }
  std::cout<<"drone_estimated_pose_topic_name="<<droneEstimatedPoseTopicName<<std::endl;
  //

  ros::param::get("~drone_Point_To_Look_Topic_Name", dronePointToLookTopicName);
  if ( dronePointToLookTopicName.length() == 0)
    {
      dronePointToLookTopicName="dronePointToLook";
    }
  ros::param::get("~drone_Aruco_Observation_Topic_Name", droneArucoObservationTopicName);
  if ( droneArucoObservationTopicName.length() == 0)
    {
      droneArucoObservationTopicName = "aruco_eye/aruco_observation";
    }

  ros::param::get("~pose_estimator", droneLedLightsPoseEstimationTopicName);
  if (droneLedLightsPoseEstimationTopicName.length() == 0)
    {
      droneLedLightsPoseEstimationTopicName = "pose_estimator/pose";
    }

  ros::param::get("~drone_Position_Ref_Command_Topic_Name", droneMissionPointCommandTopicName);
  if ( droneMissionPointCommandTopicName.length() == 0)
    {
      droneMissionPointCommandTopicName="droneMissionPoint";
    }
  std::cout << "drone_Position_Ref_Command_Topic_Name=" << droneMissionPointCommandTopicName << std::endl;

  return;
}

void DroneArucoPlannerROSModule::open(ros::NodeHandle & nIn)
{
  //Node
  DroneModule::open(nIn);

  // Parameters
  readParameters();

  // Init
  init();

  /// Services ///
  moduleIsStartedClientSrv = n.serviceClient<droneMsgsROS::askForModule>("moduleIsStarted");

  setControlModeClientSrv = n.serviceClient<droneMsgsROS::setControlMode>(std::string(MODULE_NAME_TRAJECTORY_CONTROLLER)+"/setControlMode");

  //// Topics ///


  droneArucoObservationSub = n.subscribe(droneArucoObservationTopicName, 1, &DroneArucoPlannerROSModule::droneArucoObservationCallback, this);
  droneEstimatedPoseSubs   = n.subscribe(droneEstimatedPoseTopicName,      1, &DroneArucoPlannerROSModule::droneEstimatedPoseCallback, this);

  //droneLedPoseSubs = n.subscribe(droneLedLightsPoseEstimationTopicName, 1, &DroneArucoPlannerROSModule::droneLedPoseEstimationCallback, this);

  drone_abs_trajectory_reference_publisher = n.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(DRONE_TRAJECTORY_CONTROLLER_INTERFACE_ABS_TRAJ_REF_CMD_PUBLICATION, 1);

  droneMissionPointCommandPub   = n.advertise<droneMsgsROS::dronePositionRefCommand>(droneMissionPointCommandTopicName, 1);
  dronePointToLookPub = n.advertise<droneMsgsROS::dronePositionRefCommand>(dronePointToLookTopicName, 10);//TODO


  /// Distance of the object from the camera
  /// front: x positive
  /// right: y positive
  /// example: (x,y) = (2,1) means that the object is 2 meters in front of the camera and 1 meter to the right
  distanceFromObjectPub = n.advertise<geometry_msgs::PointStamped>("objectDistanceFromCamera",1);

  //drone_interface.move
  droneCommandPubl=n.advertise<droneMsgsROS::droneCommand>(DRONE_CONSOLE_INTERFACE_COMMAND_DRONE_HL_COMMAND_PUBLICATION,1, true);

  //Flag of module opened
  droneModuleOpened=true;

  // Flag to autostart
  // moduleStarted=true;


  //End
  return;
}

bool DroneArucoPlannerROSModule::setControlMode(Controller_MidLevel_controlMode::controlMode new_control_mode) {
  //Prepare service message
  droneMsgsROS::setControlMode setControlModeSrv;
  //setControlModeSrv.request.controlMode.command = new_control_mode;
  setControlModeSrv.request.controlMode.command = Controller_MidLevel_controlMode::TRAJECTORY_CONTROL;
  //use service
  if(setControlModeClientSrv.call(setControlModeSrv))
    {
      ROS_INFO_STREAM("Change of mode successful: " << new_control_mode);
      return setControlModeSrv.response.ack;
    }
  else {
      ROS_ERROR_STREAM("Fail to change mode! ");
      return false;
    }

  // // Controller_MidLevel_controlMode::TRAJECTORY_CONTROL
  //  bool result = false;
  //  droneMsgsROS::setControlMode setControlModeSrv;
  //  setControlModeSrv.request.controlMode.command = Controller_MidLevel_controlMode::POSITION_CONTROL;
  //  //use service
  //  if (drone_manager->getControlModeClientSrv().call(setControlModeSrv))
  //      result = setControlModeSrv.response.ack;

  //  else
  //      result = false;


}


bool DroneArucoPlannerROSModule::publishMissionPointCommand(droneMsgsROS::dronePositionRefCommand droneMissionPointCommandIn)
{
  if(droneModuleOpened==false)
    return 0;

  droneMissionPointCommandPub.publish(droneMissionPointCommandIn);

  return true;
}

void DroneArucoPlannerROSModule::droneLedPoseEstimationCallback(const nav_msgs::Odometry &msg)
{

  if(!isStarted())
    return;


  distanceFromObjectMsg.header.stamp = ros::Time::now();


  if (msg.pose.pose.position.x != 0.0)
    {
      distanceFromObjectLastMeasurement.point.x = msg.pose.pose.position.x;
      distanceFromObjectLastMeasurement.point.y = msg.pose.pose.position.y;
      distanceFromObjectLastMeasurement.point.z = msg.pose.pose.position.z;

      distanceFromObjectMsg.point.x = - msg.pose.pose.position.y ;
      distanceFromObjectMsg.point.y = - msg.pose.pose.position.x ;
      distanceFromObjectMsg.point.z =   msg.pose.pose.position.z ;
    }
  else
    {
      distanceFromObjectMsg.point.x = - distanceFromObjectLastMeasurement.point.y ;
      distanceFromObjectMsg.point.y = - distanceFromObjectLastMeasurement.point.x ;
      distanceFromObjectMsg.point.z =   distanceFromObjectLastMeasurement.point.z ;
    }



  nav_msgs::Odometry pose;
  pose = msg;

  if ( pose.pose.pose.position.x != 0.0 )
    {
     // setReference(msg);
    }

  distanceFromObjectPub.publish(distanceFromObjectMsg);

}

void DroneArucoPlannerROSModule::droneArucoObservationCallback(const aruco_eye_msgs::MarkerListConstPtr& arucoList)
{
  if(!isStarted())
    return;



  arucoObservations.clear();
  for(unsigned int i=0; i<arucoList->markers.size(); i++)
    {
//      droneMsgsROS::Observation3D obs_msg = msg.obs[i];
//      observation.id = obs_msg.id;
//      observation.x = obs_msg.x;
//      observation.y = obs_msg.y;
//      observation.z = obs_msg.z;


      //  droneMissionPointCommandMsg.x = last_estimatedPose.x - observation.y;
      //  droneMissionPointCommandMsg.y = last_estimatedPose.y - (goalDistance - observation.x);
      //  droneMissionPointCommandMsg.z = 1.0;


      distanceFromObjectMsg.header.stamp = ros::Time::now();

      geometry_msgs::Pose poseMsg=arucoList->markers[i].pose.pose;


      if (poseMsg.position.x != 0.0 || poseMsg.position.y != 0.0 || poseMsg.position.z != 0.0)
        {
          distanceFromObjectLastMeasurement.point.x = poseMsg.position.x;
          distanceFromObjectLastMeasurement.point.y = poseMsg.position.y;
          distanceFromObjectLastMeasurement.point.z = poseMsg.position.z;

          distanceFromObjectMsg.point.x =   poseMsg.position.z;
          distanceFromObjectMsg.point.y =   poseMsg.position.x;
          distanceFromObjectMsg.point.z =   poseMsg.position.y;
        }
      else
        {
          distanceFromObjectMsg.point.x =   distanceFromObjectLastMeasurement.point.z ;
          distanceFromObjectMsg.point.y =   distanceFromObjectLastMeasurement.point.x ;
          distanceFromObjectMsg.point.z =   distanceFromObjectLastMeasurement.point.y ;
        }

      distanceFromObjectPub.publish(distanceFromObjectMsg);


      //      std::cout<<observation.x<<" "<<observation.y<<" "<<observation.z<<std::endl<<std::endl;

//      if (observation.id != goalAruco)
//        {
//          cout<<"NENENENENENENENE"<<endl;

//        }
//      else if (observation.id == goalAruco)
//        {
//          cout<<"DADADADADA"<<"    "<<counter<<endl;

//          counter++;
//          //drone_trajectory_controller.setControlMode(Controller_MidLevel_controlMode::TRAJECTORY_CONTROL);
//          // if ((counter+1)%10 == 0)
//          if(counter == 10)
//            {
//             // setReference(obs_msg);
//            }

        //}
    }
}

void DroneArucoPlannerROSModule::setReference(const nav_msgs::Odometry &msg)
{
  //  droneMissionPointCommandMsg.x = last_estimatedPose.x - observation.y;
  //  droneMissionPointCommandMsg.y = last_estimatedPose.y - (goalDistance - observation.x);
  //  droneMissionPointCommandMsg.z = 1.0;

  //  droneMissionPointCommandMsg.x = 0.0;
  //  droneMissionPointCommandMsg.y = 3.0;
  //  droneMissionPointCommandMsg.z = 1.0;

  double desired_distance, desired_height;
  desired_distance = 0.0;
  desired_height = 0.0;

  droneMissionPointCommandMsg.x = last_estimatedPose.x + ( - ( msg.pose.pose.position.y + desired_distance));
  droneMissionPointCommandMsg.y = last_estimatedPose.y + ( - ( msg.pose.pose.position.x));
  droneMissionPointCommandMsg.z = desired_height;

  std::cout<<droneMissionPointCommandMsg.x<<" "<<droneMissionPointCommandMsg.y<<" "<<droneMissionPointCommandMsg.z<<std::endl<<std::endl;

  // "o" - start controller: start EKF > start controller
  if(!isModuleStarted(module_name_ekf))
    startModule(true,module_name_ekf);

  droneCommandMsgs.command = droneMsgsROS::droneCommand::MOVE;
  publishDroneCmd();
  if (!isModuleStarted(module_name_traj))
    startModule(true,module_name_traj);

  // "9" - start trajectory controller (3D quadrilateral) using rel_trajectory_ref channel
  if (isModuleStarted(module_name_traj))
    {

      //      ////////// Requires a trayectory first!   -- droneTrajectoryController.cpp ???
      //      Controller_MidLevel_controlMode::TRAJECTORY_CONTROL:
      //                  // Requires a trayectory first!
      //                  error_ocurred = state_machine.activateTrajectoryControl();
      //                  if (!error_ocurred) {
      //                      midlevel_controller.setControlMode(mode);
      //                      setAllReferences_droneLMrT_wrt_LMrTFF(xs, ys, yaws, zs);
      //                  } else {
      //                      setControlMode(init_control_mode);
      //                      return false;
      //                  }
      //                  break;


      //setControlMode(Controller_MidLevel_controlMode::TRAJECTORY_CONTROL);

      // setControlMode(Controller_MidLevel_controlMode::TRAJECTORY_CONTROL);
      std::vector<SimpleTrajectoryWaypoint> trajectory_waypoints_out;
      int initial_checkpoint_out = 0;
      bool is_periodic_out = false;
      trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(droneMissionPointCommandMsg.x, droneMissionPointCommandMsg.y, droneMissionPointCommandMsg.z));
      publishDroneAbsTrajectoryReference( &trajectory_waypoints_out, initial_checkpoint_out, is_periodic_out);

      //setControlMode(Controller_MidLevel_controlMode::TRAJECTORY_CONTROL);

    }

  //      setControlMode(Controller_MidLevel_controlMode::TRAJECTORY_CONTROL);

  //      /// Position reference ///
  //      droneMissionPointCommandMsg.x = last_estimatedPose.x - observation.y;
  //      droneMissionPointCommandMsg.y = last_estimatedPose.y - (goalDistance - observation.x);
  //      droneMissionPointCommandMsg.z = last_estimatedPose.z;
  //      publishMissionPointCommand(droneMissionPointCommandMsg);

  //        /// Point to look ///
  //        dronePointToLookMsg.x = last_estimatedPose.x - observation.y;
  //        dronePointToLookMsg.y = last_estimatedPose.y + observation.x;
  //        dronePointToLookMsg.z = 1.0;

  //  dronePointToLookMsg.x = -1.0;
  //  dronePointToLookMsg.y = 3.0;
  //  dronePointToLookMsg.z = 1.0;

  //  publishPointToLook(dronePointToLookMsg);

}

void DroneArucoPlannerROSModule::publishDroneAbsTrajectoryReference(const std::vector<SimpleTrajectoryWaypoint> * const trajectory_waypoints_out, const int initial_checkpoint_out, const bool is_periodic_out ) {
  droneMsgsROS::dronePositionTrajectoryRefCommand drone_trajectory_reference_command;
  //    drone_trajectory_reference_command.target_frame = "drone_GMR";
  //    drone_trajectory_reference_command.reference_frame = "GFF";
  //    drone_trajectory_reference_command.YPR_system = "wYvPuR";

  //drone_trajectory_reference_command.header.stamp = ros::Time::now();
  for (std::vector<SimpleTrajectoryWaypoint>::const_iterator it = (*trajectory_waypoints_out).begin();
       it != (*trajectory_waypoints_out).end();
       ++it) {
      droneMsgsROS::dronePositionRefCommand next_waypoint;
      next_waypoint.x = it->x;
      next_waypoint.y = it->y;
      next_waypoint.z = it->z;
      drone_trajectory_reference_command.droneTrajectory.push_back(next_waypoint);
    }
  drone_trajectory_reference_command.initial_checkpoint = initial_checkpoint_out;
  drone_trajectory_reference_command.is_periodic = is_periodic_out;

  drone_abs_trajectory_reference_publisher.publish(drone_trajectory_reference_command);
}

void DroneArucoPlannerROSModule::droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr &msg) {
  last_estimatedPose = (*msg);
}


bool DroneArucoPlannerROSModule::publishPointToLook(droneMsgsROS::dronePositionRefCommand dronePointToLookIn)
{
  if(droneModuleOpened==false)
    return 0;

  dronePointToLookPub.publish(dronePointToLookIn);

  return true;
}

bool DroneArucoPlannerROSModule::publishDroneCmd()
{
  droneCommandPubl.publish(droneCommandMsgs);

  return true;
}


bool DroneArucoPlannerROSModule::isModuleStarted(string module_name_str)
{
  droneMsgsROS::askForModule srv;
  srv.request.module_name = module_name_str;

  if(!moduleIsStartedClientSrv.call(srv))
    ROS_ERROR_STREAM("Failed to call service for checking status");

  if(srv.response.ack)
    {
      //ROS_INFO_STREAM("Module is running: "<<module_name_str);
      return true;
    }
  else
    {
      ROS_INFO_STREAM("Module is NOT running: "<<module_name_str);
      return false;
    }
}



bool DroneArucoPlannerROSModule::startModule(bool block_execution_until_state_estimator_is_started,string module_name_str)
{
  ros::ServiceClient startClientSrv=n.serviceClient<std_srvs::Empty>(module_name_str+"/start");

  bool module_has_been_started = false;
  bool isStartedResp = isModuleStarted(module_name_str);
  if ( isStartedResp == false)
    {
      bool success = startClientSrv.call(emptySrv);
      if (success)
        {
          module_has_been_started = true;
          ROS_INFO_STREAM("Success run: " <<  module_name_str);
        }
      else
        {
          module_has_been_started = false;
          ROS_ERROR_STREAM("Failed to run: " << module_name_str);
        }
    }
  else if ( isStartedResp == true)
    module_has_been_started = true;
  else
    module_has_been_started = false;

  if ( module_has_been_started && block_execution_until_state_estimator_is_started )
    {
      while (!isModuleStarted(module_name_str))
        {
          cout<<"ovde "<<endl;
          ros::spinOnce();
          ros::Duration(0.05).sleep();
        }
    }
  return module_has_been_started;
}


///ubaci ovo umesto da se pritisne 9 i izbegni inicijalizaciju
///
///
///normal
///
/*
case 'o': { // start controller: start EKF > start controller
    if ( !drone_ekf_state_estimator_interface.isStarted() )
        drone_ekf_state_estimator_interface.start(true);

    ros::ServiceClient VSStopsrv =n.serviceClient<std_srvs::Empty>("droneVSFollowingFromTop/stop");
    std_srvs::Empty emptySrvMsg;
    VSStopsrv.call(emptySrvMsg);

    drone_interface.drone_move();
    if ( !drone_trajectory_controller_interface.isStarted() )
        drone_trajectory_controller_interface.start();
    printw("start controller"); clrtoeol();
}

    case '9': { // start trajectory controller (3D quadrilateral) using rel_trajectory_ref channel
        if ( drone_trajectory_controller_interface.isStarted() ) {
            std::vector<SimpleTrajectoryWaypoint> trajectory_waypoints_out;
            int initial_checkpoint_out = 0;
            bool is_periodic_out = false;
            trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  0.0, 0.0, 1.3) );
            trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  0.0, 6.05, 1.3) );
            trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  2.0, 6.05, 1.3) );
            trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  2.0, 0.0, 1.3) );
            trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  0.5, 0.5, 1.3) );
            //trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  5.0, 0.0, 1.3) );
            //trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  0.5, 0.5, 1.3) );
            drone_trajectory_controller_interface.publishDroneAbsTrajecotryReference( &trajectory_waypoints_out, initial_checkpoint_out, is_periodic_out);
        }
    }

    */

